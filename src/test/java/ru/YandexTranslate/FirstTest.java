package ru.YandexTranslate;

import Entities.Translations;
import Gateway.YandexTranslateGateway;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.logging.Logger;

public class FirstTest {
    String folderId = "b1gglefphhip7g53t2gk";
    String texts = "Hello World!";
    String targetLanguageCode = "ru";
    private static Logger log = Logger.getLogger(FirstTest.class.getName());

     @Test
    public void firstTest() {
         log.info("Начало теста");
         Translations fromGateway = YandexTranslateGateway.getTranslate(folderId, texts, targetLanguageCode);

         log.info("Первая проверка");
         Assertions.assertEquals("Привет, мир!", fromGateway.translations.get(0).text);
         log.info("Вторая проверка");
         Assertions.assertEquals("Всем Привет!", fromGateway.translations.get(0).text);
         log.info("Тест успешен");

     }


}
