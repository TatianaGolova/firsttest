package Gateway;

import Entities.Translations;
import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import java.util.logging.Logger;

public class YandexTranslateGateway {
    static String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    static String TOKEN = "Bearer t1.9euelZrNi5SJzZiejonMm5aNlIrKjO3rnpWams3MiYqYl4vNzIqXkpmMi87l9Pc6ZSF3-e8hRhXA3fT3ehMfd_nvIUYVwA.FmtwTDjpg-NIC8J5cpNexcSetWAt9mrdiqX90fdaMqq0hYZFDQd5bihC7Apsf87xONS6WGwxlaCju17Aohs1AA";
    private static Logger log = Logger.getLogger(YandexTranslateGateway.class.getName());

    public static Translations getTranslate(String folderId, String texts, String targetLanguageCode) {

        Gson gson = new Gson();

        log.info("Создание тела запроса");
        JSONObject requestParams = new JSONObject();
        requestParams.put("folderId", folderId);
        requestParams.put("texts", texts);
        requestParams.put("targetLanguageCode", targetLanguageCode);
        log.info("Тело запроса: " + requestParams);

        log.info("Формирование и отправка POST запроса");
        RequestSpecification request = RestAssured.given();
        request.header("Authorization", TOKEN);
        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());
        Response response = request.post(URL);

        String body = response.getBody().asString();
        log.info("Тело ответа: \n" + body);

        return gson.fromJson(body, Translations.class);

    }
}
